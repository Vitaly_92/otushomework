﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkOperators
{
    /// <summary>
    /// Сущность "Станция" или "Остановка".
    /// </summary>
    class Station
    {
        /// <summary> 
        /// Название станции (вокзала, остановки) 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Выезд со станции
        /// </summary>
        public DateTime DepartureTime { get; set; }

        /// <summary>
        /// Прибытие на станцию
        /// </summary>
        public DateTime ArrivalTime { get; set; }


        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="name"></param>
        /// <param name="departureTime"></param>
        /// <param name="arrivalTime"></param>
        public Station(string name = "Unknown",
               DateTime departureTime = new DateTime(),
               DateTime arrivalTime = new DateTime())
        {
            Name = name;
            DepartureTime = departureTime;
            ArrivalTime = arrivalTime;
        }

        /// <summary>
        /// Сравнение двух станций полностью
        /// </summary>
        /// <returns>true - полностью равны</returns>
        public static bool operator ==(Station station1, Station station2)
        {
            return (station1.ArrivalTime == station2.ArrivalTime &&
                    station1.DepartureTime == station2.DepartureTime &&
                    station1.Name == station2.Name);
        }
        /// <summary>
        /// Не равен хотя бы одно свойство
        /// </summary>
        public static bool operator !=(Station station1, Station station2)
        {
            return (station1.ArrivalTime != station2.ArrivalTime ||
                    station1.DepartureTime != station2.DepartureTime ||
                    station1.Name != station2.Name);
        }

        /// <summary>
        /// Сравнение имени
        /// </summary>
        public static bool operator ==(Station station1, string name)
        {
            return station1.Name == name;
        }

        /// <summary>
        /// Сравнение имени
        /// </summary>
        public static bool operator !=(Station station1, string name)
        {
            return station1.Name != name;
        }

        public override string ToString()
        {
            return $"Станция: {this.Name}\tвремя прибытия: {this.ArrivalTime}\tвремя отправления: {this.DepartureTime}";
        }

    }
}