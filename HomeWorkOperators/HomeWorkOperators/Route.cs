﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkOperators
{
    /// <summary> Класс "Путь следования".
    /// </summary>
    class Route
    {
        private bool inTransit;

        /// <summary>
        /// True - транспортное средство в пути, false - стоит на станции, при этом LastDepartureStation должен быть равен текущей станции
        /// </summary>
        public bool InTransit
        {
            get { return inTransit; }
            set
            {
                inTransit = value;
            }
        }

        //избегаем магических чисел
        const int MaxStationCount = 10;
        private Station[] stations = new Station[MaxStationCount];

        public Route()
        {
            for (int i = 0; i < stations.Length; i++)
            {
                stations[i] = new Station();
            }
        }

        public Station this[int index]
        {
            get
            {
                return stations[index];
            }
            set
            {
                stations[index] = value;
            }
        }

        #region Methods
        /// <summary> Возращает последнюю станцию отправки в заданное время</summary>
        /// <param name="dateTime"> время поиска по маршруту </param>
        public Station GetDepartureStationFromTime(DateTime dateTime)
        {
            return this.stations.FirstOrDefault(x => x.DepartureTime.Ticks < dateTime.Ticks && x.ArrivalTime.Ticks >= dateTime.Ticks);
        }

        /// <summary> Возращает ближайшую станцию прибытия в заданное время</summary>
        /// <param name="dateTime"> время поиска по маршруту </param>
        public Station GetArrivalStationFromTime(DateTime dateTime)
        {
            return this.stations.FirstOrDefault(x => x.DepartureTime.Ticks > dateTime.Ticks && x.ArrivalTime.Ticks <= dateTime.Ticks);
        }
        #endregion
    }
}
