﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkOperators
{
    class Program
    {
        static void Main(string[] args)
        {
            //проверка работы программы
            Route route1 = new Route();
            for (int i = 0; i < 5;i++)
            {
                Console.WriteLine(route1[i]);
            }
            Console.WriteLine(Environment.NewLine);
            route1[0] = new Station("Москва", new DateTime(2019, 11, 11, 1, 1, 1), new DateTime(2019, 11, 11, 1, 1, 1));
            route1[1] = new Station("Москва", new DateTime(2019, 11, 11, 1, 1, 1), new DateTime(2019, 11, 11, 1, 1, 1));
            route1[2] = new Station("Ямки", new DateTime(2019, 11, 11, 2, 1, 1), new DateTime(2019, 11, 11, 3, 11, 1));
            route1[3] = new Station("Ростов", new DateTime(2019, 11, 11, 3, 1, 1), new DateTime(2019, 11, 11, 4, 11, 1));
            route1[4] = new Station("Сочи", new DateTime(2019, 11, 11, 4, 1, 1), new DateTime(2019, 11, 11, 5, 11, 1));

            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine(route1[i]);
            }

            Console.WriteLine($"{Environment.NewLine}Сравниваем 1 и 2 элементы{Environment.NewLine}");

            for (int i = 0; i < 2; i++)
            {
                Console.WriteLine(route1[i] == route1[i+1] ? $"{i} и {i+1} элементы равны" : $"{i} и {i+1} элементы не равны");
            }

        }
    }
}
